<?php

namespace App\Http\Controllers\Api;

use App\Book;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBookRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BookController extends Controller
{
    //
    public function index() {
        return response()->json(Book::with(['category'])->get());
    }

    public function store(StoreBookRequest $request) {
        Book::create($request->all());
        return response()->json(['message' => 'Book saved successfully!']);
    }

    public function update(StoreBookRequest $request, $id) {
        try {
            $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            $book->update($request->all());

            $updated_book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            return response()->json(['message' => 'Book updated successfully!', 'book' => $updated_book]);
            
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Book not found'], 404);
        }
    }

    public function destroy($id) {
        try {
            $book = Book::where('id', $id)->firstOrFail();
            $book->delete();

            return response()->json(['message' => 'Book deleted successfully!']);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Book not found'], 404);
        }
    }

}
