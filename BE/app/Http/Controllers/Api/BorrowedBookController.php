<?php

namespace App\Http\Controllers\Api;

use App\BorrowedBook;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBorrowedBookRequest;

class BorrowedBookController extends Controller
{
    //
    public function index() {
        return response()->json(BorrowedBook::with([
            'patron', 'book', 'book.category'
        ])->get());
    }

    public function store(StoreBorrowedBookRequest $request) {

        $create_borrowed = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));
        $borrowedbook = BorrowedBook::with(['book'])->find($create_borrowed->id);
        $copies = $borrowedbook->book->copies - $request->copies;
        $borrowedbook->book->update(['copies' => $copies]);

        return response()->json(['message' => 'Book borrowed successfully', 'borrowedbook' => $borrowedbook]);
    }
}
