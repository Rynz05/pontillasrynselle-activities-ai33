<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePatronRequest;
use App\Patron;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PatronController extends Controller
{
    //
    public function index() {
        return response()->json(Patron::all());

    }

    public function store(StorePatronRequest $request) {

        Patron::create($request->all());
        return response()->json(['message' => 'Patron saved']);
    }

    public function update(StorePatronRequest $request, $id) {
        try {
            $patron = Patron::findOrFail($id);
            $patron->update($request->all());

            return response()->json(['message' => 'Patron updated', 'patron' => $patron]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Patron not found'], 404);
        }
    }

    public function destroy($id) {
        try {
            $patron = Patron::where('id', $id)->firstOrFail();
            $patron->delete();

            return response()->json(['message' => 'Patron deleted successfully!']);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Patron not found'], 404);
        }
    }

}
