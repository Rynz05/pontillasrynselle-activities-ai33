<?php

namespace App\Http\Controllers\Api;

use App\BorrowedBook;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReturnedBookRequest;
use App\ReturnedBook;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ReturnedBookController extends Controller
{
    //
    public function index() {
        return response()->json(ReturnedBook::with(['book', 'patron', 'book.category'])->get());
    }

    public function store(StoreReturnedBookRequest $request) {
        try {
            $borrowedbook = BorrowedBook::where([
                ['book_id', $request->book_id],
                ['patron_id', $request->patron_id],
            ])->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Book not found'], 404);
        }

        if (!empty($borrowedbook)) 
        {
            if ($borrowedbook->copies == $request->copies) {
                
                $borrowedbook->delete();
            } else {
                $borrowedbook->update(['copies' => $borrowedbook->copies - $request->copies]);
            }

            $create_returned = ReturnedBook::create($request->only(['book_id', 'copies', 'patron_id']));
            $returnedbook = ReturnedBook::with(['book'])->find($create_returned->id);
            $copies = $returnedbook->book->copies + $request->copies;

            $returnedbook->book->update(['copies' => $copies]);
            return response()->json(['message' => 'Book returned successfully!', 'book' => $returnedbook]);
        }
    }
}
