<?php

namespace App\Http\Requests;

use App\Book;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class StoreBorrowedBookRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * 
     * @return array
     */
    public function rules()
    {
        $book = Book::find(request()->get('book_id'));
        $copies = !empty($book) ? $book->copies : request()->get('copies');
        
        return [
            'book_id' => 'bail|required|exists:books,id',
            'copies' =>   "bail|required|numeric|min:1|max:{$copies}",
            'patron_id' => 'exists:patrons,id',
        ];
    }

    /**
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'book_id.exists' => 'Book doesn\'t exist in the database',
            'copies.max' => 'Copies exceeded the total copies of book',
            'copies.min' => 'Copies must at least greater than 1',
            'patron_id.exists' => 'Patron doesn\'t exist in the database'
        ];
    }

    //Returns a json reponse with a status code of 422
    public function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
    }
}
