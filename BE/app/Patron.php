<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    //
    protected $guarded = [];

    
    public function borrowedbook(){
        return $this->hasMany(BorrowedBook::class, 'patron_id', 'id');
    }

    public function returnedbook(){
        return $this->hasMany(ReturnedBook::class, 'patron_id', 'id');
    }
}
