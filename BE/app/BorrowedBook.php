<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowedBook extends Model
{
    //
    protected $guarded = [];
    
    public function patron(){
        return $this->belongsTo(Patron::class, 'patron_id', 'id');
    }

    public function book(){
        return $this->belongsTo(Book::class, 'book_id', 'id');
    }
}
