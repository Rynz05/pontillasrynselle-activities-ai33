<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    protected $guarded = [];

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function borrowed(){
        return $this->hasMany(BorrowedBook::class, 'book_id', 'id');
    }

    public function returned(){
        return $this->hasMany(ReturnedBook::class, 'book_id', 'id');
    }
}
