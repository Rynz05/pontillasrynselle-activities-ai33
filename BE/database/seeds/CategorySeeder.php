<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = [
            ['category' => 'Fiction'],
            ['category' => 'Horror'],
            ['category' => 'Novel'],
            ['category' => 'Adventure'],
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
