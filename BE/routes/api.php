<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'namespace' => 'Api',
    'middleware' => 'cors'
], function ($router){
    //Category
    Route::get('categories', 'CategoryController@index');
    //Book
    Route::get('books', 'BookController@index');
    Route::post('books', 'BookController@store');
    Route::put('books/{id}', 'BookController@update');
    Route::delete('books/{id}', 'BookController@destroy');
    //Patron
    Route::get('patrons', 'PatronController@index');
    Route::post('patrons', 'PatronController@store');
    Route::put('patrons/{id}', 'PatronController@update');
    Route::delete('patrons/{id}', 'PatronController@destroy');
    //Borrowed Book
    Route::get('borrowedbooks', 'BorrowedBookController@index');
    Route::post('borrowedbooks', 'BorrowedBookController@store');
    //Returned Book
    Route::get('returnedbooks', 'ReturnedBookController@index');
    Route::post('returnedbooks', 'ReturnedBookController@store');
});

