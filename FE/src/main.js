import Vue from "vue";
import App from "../src/App.vue";
import toastr from "toastr";
import { BootstrapVue } from "bootstrap-vue";
import router from "./routes/router";
import store from "./store";
import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "toastr/build/toastr.css";
import "./assets/css/style.css";

Vue.use(VueFilterDateFormat);
Vue.use(BootstrapVue);

toastr.options = {
  closeButton: false,
  debug: false,
  newestOnTop: false,
  progressBar: false,
  positionClass: "toast-top-right",
  preventDuplicates: false,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut",
};

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
