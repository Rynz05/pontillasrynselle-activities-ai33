export default {
    data() {
   return {
     Form: {
       bookname: "",
       bauthor: "",
       bcopies: "",
       bcategory: "",
       bookDateRegistered: new Date(),
     },
     bookModel: [],
     Selectedbook: "",
     Search: "",
   };
 },
 methods: {
   Addbook() {
     this.bookModel.unshift(this.Form);
     console.log(this.form);
     this.HideModal();
   },
   Updatebook(index) {
     console.log(index);
     this.bookModel.splice(index, 1, this.Form);

     this.HideModal();
   },
   ToBeUpdated(index) {
     this.Selectedbook = index;
     this.Form.bookname = this.bookModel[index].bookname;
     this.Form.bauthor = this.bookModel[index].bauthor;
     this.Form.bcopies = this.bookModel[index].bcopies;
     this.Form.bcategory = this.bookModel[index].bcategory;
   },
   Deletebook(index) {
     this.bookModel.splice(index, 1);

     this.HideModal();
   },
   HideModal() {
     this.$refs.modal.hide();
     this.FormReset();
   },
   FormReset() {
     this.Form = {
       bookname: "",
       bauthor: "",
       bcopies: "",
       bcategory: "",
       bookDateRegistered: new Date(),
     };

     this.Selectedbook = "";
   },
 },
 computed: {
   bookModelFilter() {
     return this.bookModel.filter((book) => {
       return this.Search.toLowerCase()
         .split(" ")
         .every((v) => book.bookname.toLowerCase().includes(v));
     });
   },
 },
};
