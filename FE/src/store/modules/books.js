import axios from "../../config";
export default {
  state: {
    books: [],
    categories: [],
    borrowedbooks: [],
    returnedbooks: [],
  },
  mutations: {
    setBooks: (state, books) => (state.books = books),
    setCategories: (state, categories) => (state.categories = categories),
    setBorrowedBooks: (state, borrowedbooks) =>
      (state.borrowedbooks = borrowedbooks),
    setReturnedBooks: (state, returnedbooks) =>
      (state.returnedbooks = returnedbooks),
  },
  actions: {
    async addBook({ commit }, data) {
      const response = await axios
        .post("books", data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async updateBook({ commit }, { data, id }) {
      const response = await axios
        .put(`books/${id}`, data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async deleteBook({ commit }, id) {
      const response = await axios
        .delete(`books/${id}`)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async fetchBooks({ commit }) {
      await axios
        .get("books")
        .then((res) => {
          commit("setBooks", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },
    async fetchCategories({ commit }) {
      await axios
        .get("categories")
        .then((res) => {
          commit("setCategories", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },
    // Borrowed Books
    async fetchBorrowedBooks({ commit }) {
      await axios
        .get("borrowedbooks")
        .then((res) => {
          commit("setBorrowedBooks", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },

    async borrowBook({ commit }, book) {
      const response = await axios
        .post("borrowedbooks", book)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },

    // Returned Books
    async fetchReturnedBooks({ commit }) {
      await axios
        .get("returnedbooks")
        .then((res) => {
          commit("setReturnedBooks", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },

    async returnBook({ commit }, book) {
      const response = await axios
        .post("returnedbooks", book)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
  },
  getters: {
    getBooks: (state) => state.books,
    getCategories: (state) => state.categories,
    getBorrowedBooks: (state) => state.borrowedbooks,
    getReturnedBooks: (state) => state.returnedbooks,
  },
};
