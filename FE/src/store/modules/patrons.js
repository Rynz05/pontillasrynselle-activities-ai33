import axios from "../../config";
export default {
  state: {
    patrons: [],
  },
  mutations: {
    setPatrons: (state, patrons) => (state.patrons = patrons),
  },
  actions: {
    async addPatron({ commit }, data) {
      const response = await axios
        .post("patrons", data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async updatePatron({ commit }, { data, id }) {
      const response = await axios
        .put(`patrons/${id}`, data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async deletePatron({ commit }, id) {
      const response = await axios
        .delete(`patrons/${id}`)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
    async fetchPatrons({ commit }) {
      await axios
        .get("patrons")
        .then((res) => {
          commit("setPatrons", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },
  },
  getters: {
    getPatrons: (state) => state.patrons,
  },
};
