import Vue from "vue";
import Vuex from "vuex";
import patrons from "./modules/patrons";
import books from "./modules/books";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { patrons, books },
});
