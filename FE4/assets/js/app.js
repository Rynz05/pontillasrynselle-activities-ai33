
var ctx = document.getElementById('myBar').getContext('2d');
var myBar = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['January', 'February', 'March', 'April', 'May'],
    datasets: [{
      label: 'Borrowed',
      data: [50, 80, 30, 20, 60, 90],
      backgroundColor: '#F6D340',
    },
    {
      label: 'Returned',
      data: [20, 60, 30, 10, 40, 90],
      backgroundColor: '#F6D365',
    }
  ],
  },
  options: {
	  legend: {
      position: "bottom",
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

var ctx2 = document.getElementById("myPie").getContext("2d");
var myPie = new Chart(ctx2, {
  type: "doughnut",
  data: {
    labels: ["IT Books", "Sinesosyedad", "Programming", "Math", "Discreet Math"],
    datasets: [
      {
        data: [37,22, 10, 15, 20],
        backgroundColor: [
          "#F6D310",
		      "#F36D65",
          "#F36D10",
          "#F10D20",
          "#F20D40"
          ,
        ],
      },
    ],
  },
  
  options: {
    responsive: true,
    legend: {
      position: "bottom",
    },
    animation: {
      animateScale: true,
      animateRotate: true,
    },
  },
});


function myFunction() {
  var txt;
  var r = confirm("Are You Sure?");
  
  document.getElementById("cool").innerHTML = txt;
}

